Tasks = new Mongo.Collection("tasks");
if (Meteor.isClient) {
  // counter starts at 0
  Session.setDefault('counter', 0);

  Template.hello.helpers({
    counter: function () {
      return Session.get('counter');
    }
  });

   
 
   
 

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    }
  });

   Template.body.helpers({
    // tasks: [
    //   { text: "This is task 1" },
    //   { text: "This is task 2" },
    //   { text: "This is task 3" }
    // ]
    tasks: function () {
      // Show newest tasks at the top
      //return Tasks.find({}, {sort: {createdAt: -1}});
      return Tasks.find({});
    }
  });

   Template.body.events({
    "submit .new-task": function (event) {
      // Prevent default browser form submit
      event.preventDefault();
      console.log(event.target.text.value);
      // Get value from form element
      var text = event.target.text.value;
      // var added_by = event.target.added_by.value;
      var added_by = "ali"
 
      // Insert a task into the collection
      Tasks.insert({
        text: text,
        added_by: added_by,
        createdAt: new Date() // current time
      });
 
      // Clear form
      event.target.text.value = "";
      //event.target.added_by.value = "";
    }
  });

  


    
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
